package edu.ada.micronaut.controller;

public interface AuthController {

    String checkUser(String userName, String password);
}
