package edu.ada.micronaut.controller.impl;

import edu.ada.micronaut.controller.AuthController;
import edu.ada.micronaut.service.impl.TokenGenerator;

import javax.inject.Inject;

public class AuthControllerImpl implements AuthController {

    @Inject
    private TokenGenerator tokenGenerator;

    @Override
    public String checkUser(String userName, String password) {

        if(userName.equals("Admin") && password.equals("Admin")){
           String token=tokenGenerator.getToken(20);
            return token;
        }
        else {
            return "";
        }
    }
}
