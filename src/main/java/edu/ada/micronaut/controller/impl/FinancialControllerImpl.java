package edu.ada.micronaut.controller.impl;

import edu.ada.micronaut.controller.FinancialController;
import edu.ada.micronaut.service.FinancialService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
/**
 * In order to get Price of stock User should enter Authorized userName and Password
 * In our case it is Username: Admin and Password Admin
 * if this fields are correct code will generate random Token with length of 20
 * In controller I check whether token is generated or not and then give output
 * I faced a lot of problems while using Micronaut this is why I did not create UserEntity , REpo , Login , Registration and etc just for token.
 * As you knew from my old assignments I can easily do these things but because of the Errors I got from micronaut I cant do them.I should finish other deadlines :(
 * I just wanted to say sorry for that :)
 **/
@Controller("/financial")
public class FinancialControllerImpl implements FinancialController {

    protected static final Logger logger = LoggerFactory.getLogger(FinancialControllerImpl.class);

    @Inject
    private FinancialService financialService;

    @Inject
    AuthControllerImpl authController;

    @Override
    @Get
    @Produces(MediaType.APPLICATION_JSON)
    public Object getFinancialData(
            @QueryValue("provider") String financial_data_provider,
            @QueryValue("stock_index") String stock_index,
            @QueryValue("userName") String userName,
            @QueryValue("userPassword") String userPassword
            ) {


        String[] keywords= stock_index.split(",");
        String token=authController.checkUser(userName,userPassword);
        for(int i=0; i<keywords.length;i++)
            keywords[i]=keywords[i].toUpperCase();
        if(token.length()>0 && financial_data_provider.equals("yahoo")) {
            System.out.println(token);
                return financialService.getFinancialData(keywords);
        } else {
            return HttpResponse.badRequest("Provider Name Is Invalid OR you are not authorized user ");
        }

    }

}
