package edu.ada.micronaut.service.impl;

import edu.ada.micronaut.service.FinancialService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;

import javax.inject.Singleton;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class YahooFinanceServiceImpl implements FinancialService {

    protected static final Logger logger = LoggerFactory.getLogger(YahooFinanceServiceImpl.class);



    @Override
    public Object getFinancialData(String [] stock_index) {

        BigDecimal price = BigDecimal.ZERO;
        HashMap<String, BigDecimal> keywordAndprice =new HashMap<String,BigDecimal>();
        try {
            if(stock_index.length==1){
                Stock stock =YahooFinance.get(stock_index[0]);
                price=stock.getQuote(true).getPrice();
                keywordAndprice.put(stock_index[0],price);
            } else {
                Map<String,Stock>keywordMap=YahooFinance.get(stock_index);
                for (String keywordIndex : keywordMap.keySet()){
                    BigDecimal keywordPrice = keywordMap.get(keywordIndex).getQuote(true).getPrice();
                    keywordAndprice.put(keywordIndex,keywordPrice);
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return keywordAndprice;
    }

}

